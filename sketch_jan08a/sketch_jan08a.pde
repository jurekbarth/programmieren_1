void setup() {
  // Variable wird eingeführt mit dem Namen spiel1 vom Typ klasse_spielen
  klasse_spielen spiel1;

  // Variable spiel1 wird initialisiert
  spiel1 = new klasse_spielen();

  spiel1.drop(1, 3);

  spiel1.auslesen();
}

class klasse_spielen {
  // Variablen [lochnummer][zustand][partei]
  int[][][] spielbrett;

  //Konstruktor
  klasse_spielen() {
    // Spielbrett Array wird inizialisiert
    spielbrett = new int[7][6][2];
  }
  void drop(int partei, int lochnummer) {
    for (int i=5; i>0; i--) {
      if (spielbrett[lochnummer][i][partei] == 0) {
        spielbrett[lochnummer][i][partei] = 1;
        return;
      }
    }
  }
  void auslesen() {
    for (int i = 0; i < spielbrett.length; i ++) {
      for (int b = 0; i < 6; i++) {
        println(spielbrett[i][b][0]);
      }
    }
  }
}

