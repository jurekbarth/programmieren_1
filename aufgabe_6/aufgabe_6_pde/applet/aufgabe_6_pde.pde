/* Aufgabe: 6 Abstrake Muster
 Name: Jurek Barth
 Martrikel: 245357
 Datum: 15.11.13
 
 Hiermit versichere ich,dass ich diesen Code selbst geschrieben habe.
 Er wurde nicht kopiert und auch nich diktiert.
 */
void setup() {
  size(400, 400);
  background(random(0, 255), random(0, 255), random(0, 255));
  generatePicture();
} // Setup END

void generatePicture() {
  for (int i= 0; i<100; i++) {
    float x = random(20,380);
    float y = random(20,380);
    int form = int(random(1,4));
    float formSize = random(20, 40);
    color c = color(random(0, 255), random(0, 255), random(0, 255));
    println(form);
    drawShape(x, y, form, formSize, c);
  }
} // generate Picture END

void drawShape(float x, float y, int form, float formSize, color c) {
  fill(c);

  switch(form) {
  case 1:
    ellipse(x, y, formSize, formSize);
    break;
  case 2:
    triangle(x-(formSize/2), y, x+(formSize/2), y, x, y-(formSize/2));
    break;
  case 3:
    rect(x-formSize/2, y-(formSize/2), formSize, formSize);
    break;
  }
} // Draw END

