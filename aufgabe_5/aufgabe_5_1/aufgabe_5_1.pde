/* Aufgabe: 5.1 Antarktis wird Methode
 Name: Jurek Barth
 Martrikel: 245357
 Datum: 05.11.13
 
 Hiermit versichere ich,dass ich diesen Code selbst geschrieben habe.
 Er wurde nicht kopiert und auch nich diktiert.
 */
void setup() {
  size(800, 600);
  noStroke();
  smooth();
  // Define Colors:
  color sky = color(#AEDAF8);
  color white = color(#ffffff);
  color snow = color(#F4F7FA);
  color black = color(#000000);
  color orange = color(#E5861F);
  color sea = color(#8AABDA);
  color grey = color(#454545);
  color yellow = color(#FCF238);
  int i = 0;
  while ( i != 200) {
    fill(i, 218, 248);
    rect(0, i, 800, 1);
    i ++;
  }
  fill(sea);
  rect(0, 200, 800, 200);
  fill(snow);
  rect(0, 400, 800, 200);
  // Sonne
  fill(yellow);
  ellipse(700, 50, 70, 70);
  // Berge
  fill(snow);
  triangle(50, 200, 250, 200, 150, 50);
  triangle(150, 200, 350, 200, 250, 40);
  // Schneescholle
  fill(snow);
  ellipse(300, 310, 130, 90);

  // Erstelle Wolken
  Wolke w1 = new Wolke(500, 70);
  Wolke w2 = new Wolke(300, 20);
  
  i = 0;
  fill(snow);
  stroke(white);
  float x = 0;
  float y = 0;
  while (i<400) {
    x = random(0, 800);
    y = random(0, 600);
    ellipse(x, y, 10, 10);
    i ++;
  }
  
  // Erstelle Pinguine
  i = 0;
  noStroke();
  while (i <5) {
    if (i==0) {
      Pinguin p1 = new Pinguin(100, 500);
    };
    if (i==1) {
      Pinguin p2 = new Pinguin(300, 540);
    };
    if (i==2) {
      Pinguin p3 = new Pinguin(500, 400);
    };
    if (i==3) {
      Pinguin p4 = new Pinguin(300, 300);
    };
    if (i==4) {
      Pinguin p5 = new Pinguin(700, 430);
    };
    i++;
  }
}

// Die Wolkenklasse
class Wolke {
  int a, b;
  color white = color(#ffffff);
  Wolke (int x, int y) {
    fill(white);
    ellipse (x+50, y+50, 100, 60);
    ellipse (x+100, y+40, 80, 70);
    ellipse (x+60, y+20, 55, 40);
  }
}

// Die Pinguinklasse
class Pinguin {
  float x, y;
  color white = color(#ffffff);
  color orange = color(#E5861F);
  color black = color(#000000);
  color grey = color(#454545);
  String text;
  Pinguin (float x, float y) {
    fill(black);
    // Fuesse
    arc(x, y, 100, 60, PI, PI*2);
    // Arme
    ellipse(x-30, y-70, 70, 70);
    ellipse(x+30, y-70, 70, 70);
    // Kopf
    ellipse(x, y-110, 90, 70);
    // Bauch
    fill(white);
    ellipse(x, y-60, 100, 120);
    // Augen
    ellipse(x-18, y-120, 30, 30);
    ellipse(x+18, y-120, 30, 30);
    // Pupille
    fill(grey);
    ellipse(x-15, y-120, 5, 5);
    ellipse(x+15, y-120, 5, 5);
    // Zunge
    fill(orange);
    ellipse(x, y-115, 10, 10);
    // Nase
    fill(grey);
    arc(x, y-115, 22, 22, PI, PI*2);
    text = "x: " + x + " " + "y: " + y;
    text(text, x-50, y+20);
  }
}


