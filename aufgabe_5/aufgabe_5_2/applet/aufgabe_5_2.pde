/* Aufgabe: 5.2 Roemische Zahlen
 Name: Jurek Barth
 Martrikel: 245357
 Datum: 05.11.13
 
 Hiermit versichere ich,dass ich diesen Code selbst geschrieben habe.
 Er wurde nicht kopiert und auch nich diktiert.
 */

size (400, 400);
// Random Number
float floatNumber = random(0, 4999);
int number = int(floatNumber);

//Create Strings for output
String output = "Die Zahl "+number+" heisst roemisch geschrieben";

// All roman numbers saved in an array, also special "number" CM CD etc. 4*C = CM 
String[] texts = { "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I" };
// All Values for the Roman numbers, same Index
int[] values = { 1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1 };
// New String to print it later...
String rom = "";

// For Loop, you can use while loop  instead, but its cleaner
for (int i = 0; number !=0; i++) {
  // while Number >= Value --> Number = Number -  Value
  while(number >= values[i]) {
    number = number - values[i];
    // rom is the final String, it always appends the der Text of the number to the String
    rom = rom + texts[i];
  }
}

// Prints the output
fill(#000000);
text(output, 10, 100);
text(rom, 10, 130);


