/* Aufgabe: 4.2 while-Schleifen und if-Bedingungen
Name: Jurek Barth
Martrikel: 245357
Datum: 01.11.13

Hiermit versichere ich,dass ich diesen Code selbst geschrieben habe.
Er wurde nicht kopiert und auch nich diktiert.

Naja es entsteht ein Muster oder was wollt ihr hoeren?
Vielleicht wolltet ihr auch hoeren, dass es mega anpassbar ist
und voll toll ist, solange der Kreisdurchmesser nicht groesser ist
als der Kreisabstand.
*/
size(400, 400);
smooth();
noStroke();
int distCirc = 10; //Change Distance
int sizeCirc = 10; // Change Circle Size
int sizeHeightCirc = sizeCirc; // Initializing Current Circle Height
int sizeWidthCirc = sizeCirc; // Initializing Current Circle Width
float spacing = distCirc+sizeCirc; // xn - xn+1
float margin = (width % spacing)/2; // margin
float x_start = margin + (spacing/2); // x Startpoint
float x = x_start; // Important to get x Position after resetting
float y = x; // Set y = x
int row = width / (distCirc+sizeCirc); // Circles in one Row
int row_count = 0; // How many circles are drawn in one Rpw
int i = row*row; // Total Circles
int m = 3; // Change to double Width every mte Circle
int count_m = 1; // m counter
int n = 11; // Change to double Height every mte Circle
int count_n = 1; // n counter


while (i > 0) {
  if (row == row_count) { // Check if row is full
    y = spacing + y;
    row_count = 0;
    x = x_start;
  }
  sizeWidthCirc = sizeCirc; // Reset Circle Width
  sizeHeightCirc = sizeCirc; // Reset Circle Height
  if (m == count_m && n == count_n ) { // Check if m and n is count_m, count_n
    sizeWidthCirc = sizeCirc*2;
    sizeHeightCirc = sizeCirc*2;
    count_m = 0; 
    count_n = 0; 
  };
  if (m == count_m) {
    sizeWidthCirc = sizeCirc*2;
    count_m = 0;
  } 
  if (n == count_n) {
    sizeHeightCirc = sizeCirc*2;
    count_n = 0;
  };
  fill(random(0, 255), random(0, 255), random(0, 255)); // Fill with random colors
  ellipse(x, y, sizeWidthCirc, sizeHeightCirc);
  // Counters:
  x = spacing + x;
  i --;
  row_count ++;
  count_m ++;
  count_n ++;
}

