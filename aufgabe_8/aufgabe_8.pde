/* Aufgabe: 8: Transformation, Animation, Interaktion
 Name: Jurek Barth
 Martrikel: 245357
 Datum: 30.11.13
 
 Hiermit versichere ich,dass ich diesen Code selbst geschrieben habe.
 Er wurde nicht kopiert und auch nich diktiert.
 */

// x, y, shot, size
int[][] target = new int[6][4];
// Check if draw is paused, also no cheating
boolean boolLoop;
// counter for shots
int counter;

void setup() {
  size(800, 600);
  cursor(CROSS);
  background(#ffffff);
  // initializing the points
  for (int i = 0; i < target.length; i++) {
    target[i][0] = int(random(0, 900));
    target[i][1] = int(random(100, 500));
    target[i][2] = 0;
    target[i][3] = int(random(20, 100));
  }
  boolLoop = true;
  counter = 0;
  fill(0);
  // the game manual
  text("Press spacebar to pause/continue", 310, 590);
  text("Press R to restart", 680, 590);
  loadPixels();
}

void draw() {
  updatePixels();
  fill(0);
  textSize(32);
  // prints the counter
  text(counter, 750, 40);
  // Colors
  color green = color(#73CB2F);
  color red = color(#8D0000);
  // Speed options
  int[] speed = {
    -(2*(int(counter/10)+1)), -(1*(int(counter/10)+1))
  };
  // The animation stuff...
  for (int i = 0; i<target.length; i++) {
    int size = target[i][3];
    if (target[i][2] == 1) {
      fill(green);
    } 
    else {
      fill(red);
    }
    if ( target[i][0] < -(size +20) ) {
      target[i][0] = int(random((size + 800), (size + 850)));
      target[i][1] = int(random(100, 500));
      target[i][2] = 0;
    } 
    else {
      target[i][0] += int(random(speed[0], speed[1]));
    }
    ellipse(target[i][0], target[i][1], size, size);
  }
}


// Mouse Click IO
void mouseClicked () {
  // Only works if you play, no cheating press pause and click them all
  if (boolLoop == true) {
    targetIt();
  }
}

// Check if mouse is in the "point", only draws a rect...
void targetIt() {
  for (int i = 0; i < target.length; i ++) {
    boolean boolX = false;
    boolean boolY = false;
    int cx = int(mouseX);
    int cy = int(mouseY);
    int size = target[i][3];
    for (int q = -(size/2); q <= (size/2); q ++) {
      if (cx == (target[i][0] + q)) {
        boolX = true;
      }
    }
    for (int w = -(size/2); w <= (size/2); w ++) {
      if (cy == (target[i][1] + w)) {
        boolY = true;
      }
    }
    if (boolX == true && boolY == true) {
      target[i][2] = 1;
      counter ++;
    }
  }
}

// Key pressed IO
void keyPressed() {
  // Check if spacebar pressed
  if (keyCode == 32) {
    if (boolLoop == true) {
      // Pause
      noLoop();
      boolLoop = false;
    } 
    else {
      // Play
      loop();
      boolLoop = true;
    }
  }
  // Check if R pressed
  if(keyCode == 82) {
    // Reset counter
    counter = 0;
  }
}

