counter counter;
time time;
void setup() {
  size(800, 600);
  noStroke();
  background(#ffffff);
  fill(#5F676F);
  rect(0, 400, 800, 200);
  loadPixels();
  counter = new counter();
  time = new time();
}

void draw() {
  updatePixels();
  time.time_count();
}
