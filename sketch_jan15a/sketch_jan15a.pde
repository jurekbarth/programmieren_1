void setup() {
size (400, 400);
  Spielbrett holzspielbrett = new Spielbrett();
  
  // Benutzer IDs 1 and 10
  holzspielbrett.drop(1, 1);
  holzspielbrett.drop(2, 1);
  holzspielbrett.drop(3, 10);
  holzspielbrett.drop(4, 1);
  holzspielbrett.drop(5, 1);
  holzspielbrett.drop(6, 1);
  holzspielbrett.drop(7, 1);
  
  //println(holzspielbrett.spielkoordinaten[1][5]);
  //println(holzspielbrett.spielkoordinaten[1][4]);
  
  boolean yes = holzspielbrett.check_column();
  println(yes);
}

class Spielbrett {
  int[] counter;
  int[][] spielkoordinaten;
  
  Spielbrett() {
    counter = new int[7];
    spielkoordinaten = new int[7][6];
    for (int i = 0; i < counter.length; i++) {
      counter[i] = 5;
    }
  }
  boolean drop(int spalte, int user_id) {
    int spalte_array = spalte-1;
    int field = counter[spalte_array];
    if (field < 0) {
      return false;
    }
    spielkoordinaten[spalte_array][field] = user_id;
    counter[spalte_array] --;
    return true;
  }
  
  boolean check_row() {
    boolean yes = false;
    for(int i = 0; i < spielkoordinaten.length; i++) {
      for(int z = 0; z<3; z++) {
        int summe = spielkoordinaten[i][z]+spielkoordinaten[i][z+1]+spielkoordinaten[i][z+2]+spielkoordinaten[i][z+3];
        if (summe == 40 || summe == 4) {
          yes = true;
        }
      }
    }
    return yes;
  }
  
  boolean check_column() {
    boolean yes = false;
    for(int i = 0; i < 6; i++) {
      for(int z = 0; z<4; z++) {
        int summe = spielkoordinaten[z][i]+spielkoordinaten[z+1][i]+spielkoordinaten[z+2][i]+spielkoordinaten[z+3][i];
        if (summe == 40 || summe == 4) {
          yes = true;
        }
      }
    }
    return yes;
  }
}

