/* Aufgabe: 3 Antarktis Variablen
Name: Jurek Barth
Martrikel: 245357
Datum: 23.10.13

Hiermit versichere ich,dass ich diesen Code selbst geschrieben habe.
Er wurde nicht kopiert und auch nich diktiert.
*/

size(800, 600);
noStroke();

// Define Colors:
color sky = color(#AEDAF8);
color white = color(#ffffff);
color snow = color(#F4F7FA);
color black = color(#000000);
color orange = color(#E5861F);
color sea = color(#8AABDA);
color grey = color(#454545);
color yellow = color(#FCF238);


fill(sky);
rect(0, 0, 800, 600);
fill(sea);
rect(0, 200, 800, 400);
fill(snow);
rect(0, 400, 800, 200);
// Sonne
fill(yellow);
ellipse(700, 50, 70, 70);
// Berge
fill(snow);
triangle(50, 200, 250, 200, 150, 50);
triangle(150, 200, 350, 200, 250, 40);
// Schneescholle
fill(snow);
ellipse(300, 310, 130, 90);

// Die Pinguinklasse
class Pinguin {
  int x, y;
  color white = color(#ffffff);
  color orange = color(#E5861F);
  color black = color(#000000);
  color grey = color(#454545);
  Pinguin (int x, int y) {
    fill(black);
    // Fuesse
    arc(x, y, 100, 60, PI, PI*2);
    // Arme
    ellipse(x-30, y-70, 70, 70);
    ellipse(x+30, y-70, 70, 70);
    // Kopf
    ellipse(x, y-110, 90, 70);
    // Bauch
    fill(white);
    ellipse(x, y-60, 100, 120);
    // Augen
    ellipse(x-18, y-120, 30, 30);
    ellipse(x+18, y-120, 30, 30);
    // Pupille
    fill(grey);
    ellipse(x-15, y-120, 5, 5);
    ellipse(x+15, y-120, 5, 5);
    // Zunge
    fill(orange);
    ellipse(x, y-115, 10, 10);
    // Nase
    fill(grey);
    arc(x, y-115, 22, 22, PI, PI*2);
  }
}

// Die Wolkenklasse
class Wolke {
  int a, b;
  color white = color(#ffffff);
  Wolke (int x, int y) {
    fill(white);
    ellipse (x+50, y+50, 100, 60);
    ellipse (x+100, y+40, 80, 70);
    ellipse (x+60, y+20, 55, 40);
  }
}

// Erstelle Wolken
Wolke w1 = new Wolke(500, 70);
Wolke w2 = new Wolke(300, 20);

// Erstelle Pinguine
Pinguin p1 = new Pinguin(100, 500);
Pinguin p2 = new Pinguin(300, 590);
Pinguin p3 = new Pinguin(500, 400);
Pinguin p4 = new Pinguin(300, 300);
Pinguin p5 = new Pinguin(700, 430);

// Kinderpinguin
scale(0.5);
Pinguin p6 = new Pinguin(900, 830);
scale(0.95);
Pinguin p7 = new Pinguin(500, 300);




