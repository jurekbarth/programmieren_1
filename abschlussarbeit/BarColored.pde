class BarColored extends Bar {
  int r, g, b;
  
  // Contructor with Super Constructor
  BarColored (int _height_top_bar, int _x) {
    super(_height_top_bar, _x);
    r = int(random(0, 255));
    g = int(random(0, 255));
    b = int(random(0, 255));
  }
  
  // New Paint method for Colors
  void paint() {
    fill(r, g, b);
    // top bar
    rect(x, 0, bar_width, height_top_bar);
    // bottom bar height not relevant 
    rect(x, y_bottom, bar_width, height);
  }
}

