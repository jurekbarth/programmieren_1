class Start implements ActionListener {
  PApplet x;
  // Constructor 
  Start(PApplet x) {
    this.x = x;
  }
  
  // Called by pressing the button
  void actionPerformed (ActionEvent e) {
    // removes button
    x.remove(start);
    // Starts the loop for drawings
    loop();
    // Set focus on the canvas again
    setFocusable(true);
    requestFocus();
    first_start = false;
  }
}

