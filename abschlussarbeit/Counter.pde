class Counter {
  int counter;
  // Counter constructor....
  Counter () {
    int counter = 0;
  }
  
  // Counts up by 1
  void one_up() {
    counter ++;
  }
  
  // Paints the counter on the screen method thingy
  void paint() {
    fill(#5892E3);
    textSize(32);
    text(counter, (width-50), 50);
  }
}
