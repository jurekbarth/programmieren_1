class Bar {
  int height_top_bar, y_bottom, y_goal, x;
  int goal_diameter = 130;
  int bar_width = 20;
  int speed = 1;
  
  // Bar Constructor 
  Bar (int _height_top_bar, int _x) {
    x = _x;
    height_top_bar = _height_top_bar;
    y_goal = height_top_bar;
    y_bottom = (y_goal +  goal_diameter);
  }
  
  // Setter for Bar
  void set_bar (int _height_top_bar, int _x) {
    x = _x;
    height_top_bar = _height_top_bar;
    y_goal = height_top_bar;
    y_bottom = (y_goal +  goal_diameter);
  }

  // Values to check passing the goal in Bird Class
  int[] get_goal_values() {
    int[] goal_values = {x, y_goal, goal_diameter, bar_width};
    return goal_values;
  }
  
  // Set new x Value for the Bar every frame
  void drawer() {
    if (x < -20) {
      set_bar (int(random(50, 350)), width);
    }
    x += -speed;
  }
  
  // Paints the bar
  void paint() {
    fill(#000000);
    // top bar
    rect(x, 0, bar_width, height_top_bar);
    // bottom bar height not relevant 
    rect(x, y_bottom, bar_width, height);
  }
}

