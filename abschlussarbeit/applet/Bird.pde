class Bird {
  PImage img;
  int x, y;
  int speed_up = 2;
  int speed_down = 4;
  
  // Empty Bird Contructor
  Bird () {
    x = 200;
    y = (height/2);
    img = loadImage("blue-bird.png");
  }
  
  // Fly Up method y + 2
  void fly_up() {
    y -= speed_up;
  }
  
  // Fly Down method y - 4
  void fly_down() {
    y += speed_down;
  }
  
  // Paints the bird with image
  void paint() {
    //fill(#D4382E);
    //noStroke();
    //ellipse(x, y, 20, 20);
    image(img, x-10, y-10, 20, 20);
  }
  
  // The super duper check if the bird is checking the goal
  boolean check() {
    // checks if its outside the bottom of the canvas
    boolean check = true;
    if (y > height) {
      check = false;
    }
    
    // Loops through the bars
    for (int i = 0; i < bars.length; i++) {
      // 0 = x, 1 = y, 2 = height, 3 = width. Getting values....
      int[] goal_values = bars[i].get_goal_values();
      // check if there's a goal at the birds place
      if (goal_values[0] == 200) {
        // If there is it checks if the bird is passing the goal via the y Coordinates. 
        if (y >= goal_values[1] && y <= (goal_values[1]+goal_values[2])) {
          counter.one_up();
          check = true;
        } else {
          // If not return false to game over
          check = false;
        }
      }
    }
    return check;
  }
}

