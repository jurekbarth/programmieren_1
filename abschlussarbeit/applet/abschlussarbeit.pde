/* Aufgabe: 11. Abschlussaufgabe
 Name: Jurek Barth
 Martrikel: 245357
 Datum: 16.02.2014
 
 Hiermit versichere ich,dass ich diesen Code selbst geschrieben habe.
 Er wurde nicht kopiert und auch nich diktiert.
 */

// Java Imports
import java.applet.Applet;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
Button start = new Button ("Start");

// Global Vars
Bar[] bars;
Bird bird;
Counter counter;
int bar_distance, i;
boolean restart;
boolean first_start = true;

void setup() {
  size(1200, 600);
  frameRate(100);
  restart = false;
  noStroke();
  
  // Initialize bars
  bar_distance = 200;
  int array_size = ((width / bar_distance));
  bars = new Bar[array_size];
  int start_x = 400;
  for (int i = 0; i < bars.length; i++) {
    if(i%2 == 0) {
      bars[i] = new Bar(int(random(50, 350)), start_x);
    } else {
      bars[i] = new BarColored(int(random(50, 350)), start_x);
    }
    start_x += bar_distance;
  }

  // initialize bird
  bird = new Bird();

  // initialize i
  i = 100;

  // initialize counter 
  counter = new Counter();

  // first round check 
  if (first_start == true) {
    start_game();
  } 
  else {
    loop();
  }
}

void draw() {
  background(#ffffff);
  // draw bars
  for (int i = 0; i < bars.length; i++) {
    bars[i].drawer();
    bars[i].paint();
  }
  
  // check if spacebar is pressed
  if (keyPressed) {
    if (key == 32) {
      i = 0;
    }
  }
  
  // every press let the bird fly up 30px otherwise its flying down
  if (i < 30) {
    bird.fly_up();
  } 
  else if (i > 35) {
    bird.fly_down();
  }
  bird.paint();
  counter.paint();
  
  // check if bird passes the bars
  if (bird.check() == false) {
    game_over();
    noLoop();
    counter.paint();
  }
  i ++;
  // First Game
  if (first_start == true) {
    fill(#BD4F44);
    rect(110, 60, 1000, 60);
    fill(#ffffff);
    textSize(30);
    text("Go through the parcour. Press the Spacebar to let the Bird fly high.", 120, 100);
  }
}

// Game Over screen and help to restart
void game_over() {
  // If restarts true key r is enabled, otherwise there no restart available.
  restart = true;
  textSize(50);
  background(0);
  fill(#ffffff);
  text("Game Over press R to try again", 250, 275);
}

// Checks for Restart R
void keyPressed() {
  if (key == 'r'  && restart == true || key == 'R'  && restart == true) {
    loop();
    setup();
  }
}

// Start Game Intro and Button
void start_game() {
  noLoop();
  //Start-Button
  setLayout(null);
  this.add (start);
  start.setBounds(525, 450, 150, 50);
  start.addActionListener ( new Start (this));
}

