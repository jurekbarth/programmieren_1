/* Aufgabe: 7.3. Antarkis - Es schneit
 Name: Jurek Barth
 Martrikel: 245357
 Datum: 22.11.13
 
 Hiermit versichere ich,dass ich diesen Code selbst geschrieben habe.
 Er wurde nicht kopiert und auch nich diktiert.
 */

int array_length = 400;
float [] x = new float[array_length];
float [] y = new float[array_length];
// How cloudy should it be?
int wolken_length = 8;
// Multidimensional Array First Cloud Index, Second x, y
float[][] wolken = new float[wolken_length][2];

void setup() {
  size(800, 600);
  frameRate(30);
  noStroke();
  smooth();
  // Define Colors:
  color sky = color(#AEDAF8);
  color white = color(#ffffff);
  color snow = color(#F4F7FA);
  color black = color(#000000);
  color orange = color(#E5861F);
  color sea = color(#8AABDA);
  color grey = color(#454545);
  color yellow = color(#FCF238);
  for (int i=0; i != 200; i++) {
    fill(i, 218, 248);
    rect(0, i, 800, 1);
  }
  fill(sea);
  rect(0, 200, 800, 200);
  fill(snow);
  rect(0, 400, 800, 200);
  // Sonne
  fill(yellow);
  ellipse(700, 50, 70, 70);
  // Berge
  fill(snow);
  triangle(50, 200, 250, 200, 150, 50);
  triangle(150, 200, 350, 200, 250, 40);
  // Schneescholle
  fill(snow);
  ellipse(300, 310, 130, 90);

  // Wolken Coordinates Init
  for (int i= 0;i<wolken.length; i++) {
    wolken[i][0] = random(0, 800);
    wolken[i][1] = random(40, 80);
  }
  // Snowflakes Coordinates init
  for (int i= 0;i<array_length; i++) {
    x[i] = random(0, 800);
    y[i] = random(0, 600);
  }

  // Erstelle Pinguine
  noStroke();
  int i = 0;
  while (i <5) {
    if (i==0) {
      Pinguin p1 = new Pinguin(100, 500);
    };
    if (i==1) {
      Pinguin p2 = new Pinguin(300, 540);
    };
    if (i==2) {
      Pinguin p3 = new Pinguin(500, 400);
    };
    if (i==3) {
      Pinguin p4 = new Pinguin(300, 300);
    };
    if (i==4) {
      Pinguin p5 = new Pinguin(700, 430);
    };
    i++;
  }
  loadPixels();
} // End Setup

void draw() {
  updatePixels();
  for (int i=0; i<wolken.length; i++) {
  if (wolken[i][0] > 900) {
      wolken[i][0] = random(-300, -100);
    } 
    else {
      wolken[i][0] += random(0.1, 0.5);
    }
    new Wolke(wolken[i][0], wolken[i][1]);
  }
  for (int i=0; i<array_length; i++) {
    if (y[i] > 610) {
      y[i] = random(-3, 0);
    } 
    else {
      y[i] += random(0.5, 1.5);
    }
    if (x[i] > 810) {
      x[i] = random(-3, 0);
    } 
    else {
      x[i] += random(0.1, 0.5);
    }
    fill(#ffffff);
    ellipse(x[i], y[i], 10, 10);
  }
}

// Die Wolkenklasse
class Wolke {
  //int a, b;
  color white = color(#ffffff);
  Wolke (float x, float y) {
    fill(white);
    ellipse (x+50, y+50, 100, 60);
    ellipse (x+100, y+40, 80, 70);
    ellipse (x+60, y+20, 55, 40);
  }
}

// Die Pinguinklasse
class Pinguin {
  float x, y;
  color white = color(#ffffff);
  color orange = color(#E5861F);
  color black = color(#000000);
  color grey = color(#454545);
  String text;
  Pinguin (float x, float y) {
    fill(black);
    // Fuesse
    arc(x, y, 100, 60, PI, PI*2);
    // Arme
    ellipse(x-30, y-70, 70, 70);
    ellipse(x+30, y-70, 70, 70);
    // Kopf
    ellipse(x, y-110, 90, 70);
    // Bauch
    fill(white);
    ellipse(x, y-60, 100, 120);
    // Augen
    ellipse(x-18, y-120, 30, 30);
    ellipse(x+18, y-120, 30, 30);
    // Pupille
    fill(grey);
    ellipse(x-15, y-120, 5, 5);
    ellipse(x+15, y-120, 5, 5);
    // Zunge
    fill(orange);
    ellipse(x, y-115, 10, 10);
    // Nase
    fill(grey);
    arc(x, y-115, 22, 22, PI, PI*2);
  }
}

