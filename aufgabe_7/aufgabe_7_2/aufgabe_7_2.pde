/* Aufgabe: 7.2 Roemische Zahlen im Array
 Name: Jurek Barth
 Martrikel: 245357
 Datum: 22.11.13
 
 Hiermit versichere ich,dass ich diesen Code selbst geschrieben habe.
 Er wurde nicht kopiert und auch nich diktiert.
 */

// Global Variables:
// All roman numbers saved in an array, also special "number" CM CD etc. 4*C = CM 
String[] texts = { 
  "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"
};
// All Values for the Roman numbers, same Index
int[] values = { 
  1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1
};

// Suche Zahlen
int number[] = {
  500, 600, 700, 800, 900
};
String[] rom = new String[number.length];

// Start the Setup
void setup() {
  size (400, 400);
  
  // Be happy...
  rom = getRomanNumbers(number);

  // Prints the output
  fill(#000000);
  int y = 0;
  for (int i = 0; i<number.length; i++) {
    String output = "Die Zahl "+number[i]+" heisst roemisch geschrieben";
    y += 60;
    text(output, 10, y);
    text(rom[i], 10, y+20);
  }
}

String[] getRomanNumbers(int[] number) {
  // Loops through the number array and saves the result in the rom array
  for (int i = 0; i<number.length; i++) {
    rom[i] = arabZahl(number[i]);
  }
  return rom;
}

String arabZahl(int number) {
  // New String to print it later...
  String rom = "";
  // For Loop, you can use while loop  instead, but its cleaner
  for (int i = 0; number !=0; i++) {
    // while Number >= Value --> Number = Number -  Value
    while (number >= values[i]) {
      number = number - values[i];
      // rom is the final String, it always appends the der Text of the number to the String
      rom = rom + texts[i];
    }
  }
  return rom;
}

