// Die Pinguinklasse
class Penguin {
  float x, y;
  color white = color(#ffffff);
  color orange = color(#E5861F);
  color black = color(#000000);
  color grey = color(#454545);
  // Constructor
  Penguin (float _x, float _y) {
    x = _x;
    y = _y;
  }
  // Methods 
  void paint() {
    fill(black);
    // Fuesse
    arc(x, y, 100, 60, PI, PI*2);
    // Arme
    ellipse(x-30, y-70, 70, 70);
    ellipse(x+30, y-70, 70, 70);
    // Kopf
    ellipse(x, y-110, 90, 70);
    // Bauch
    fill(white);
    ellipse(x, y-60, 100, 120);
    // Augen
    ellipse(x-18, y-120, 30, 30);
    ellipse(x+18, y-120, 30, 30);
    // Pupille
    fill(grey);
    ellipse(x-15, y-120, 5, 5);
    ellipse(x+15, y-120, 5, 5);
    // Zunge
    fill(orange);
    ellipse(x, y-115, 10, 10);
    // Nase
    fill(grey);
    arc(x, y-115, 22, 22, PI, PI*2);
  }
}

