// Die Wolkenklasse
class Cloud {
  float x, y;
  color white = color(#ffffff);
  Cloud (float _x, float _y) {
    x = _x;
    y = _y;
  }
  void paint() {
    fill(white);
    ellipse (x+50, y+50, 100, 60);
    ellipse (x+100, y+40, 80, 70);
    ellipse (x+60, y+20, 55, 40);
  }
  void animate() {
    if (x > 900) {
      x = random(-300, -100);
    } else {
      x += random(0.1, 0.5);
    }
  }
}
