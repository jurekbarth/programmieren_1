/* Aufgabe: 9. Refactoring Arktis
 Name: Jurek Barth
 Martrikel: 245357
 Datum: 08.12.13
 
 Hiermit versichere ich,dass ich diesen Code selbst geschrieben habe.
 Er wurde nicht kopiert und auch nich diktiert.
 */

Snowflake[] snowflakes;
Cloud[] clouds;
Penguin[] penguins;


void setup() {
  size(800, 600);
  frameRate(30);
  noStroke();
  smooth();

  drawBackground();
  // init all arrays
  snowflakes = new Snowflake[400];
  clouds = new Cloud[4];
  penguins = new Penguin[5];
  // Wolken Coordinates Init
  for (int i= 0;i<clouds.length; i++) {
    clouds[i] = new Cloud(random(0, 800), random(40, 80));
  }
  // Snowflakes Coordinates init
  for (int i= 0;i<snowflakes.length; i++) {
    snowflakes[i] = new Snowflake(random(0, 800), random(0, 600));
  }

  // Erstelle Pinguine
  for (int i = 0; i<penguins.length; i++) {
    switch(i) {
    case 0:
      penguins[i] = new Penguin(100, 500);
      break;
    case 1:
      penguins[i] = new Penguin(300, 540);
      break;
    case 2:
      penguins[i] = new Penguin(500, 400);
      break;
    case 3:
      penguins[i] = new Penguin(300, 300);
      break;
    case 4:
      penguins[i] = new Penguin(700, 430);
      break;
    }
    penguins[i].paint();
  }
  loadPixels();
} // End Setup

void draw() {
  updatePixels();
  for (int i= 0;i<clouds.length; i++) {
    clouds[i].animate();
    clouds[i].paint();
  }
  for (int i= 0;i<snowflakes.length; i++) {
    snowflakes[i].animate();
    snowflakes[i].paint();
  }
}

void drawBackground() {
  color sky = color(#AEDAF8);
  color white = color(#ffffff);
  color snow = color(#F4F7FA);
  color black = color(#000000);
  color orange = color(#E5861F);
  color sea = color(#8AABDA);
  color grey = color(#454545);
  color yellow = color(#FCF238);
  for (int i=0; i != 200; i++) {
    fill(i, 218, 248);
    rect(0, i, 800, 1);
  }
  fill(sea);
  rect(0, 200, 800, 200);
  fill(snow);
  rect(0, 400, 800, 200);
  // Sonne
  fill(yellow);
  ellipse(700, 50, 70, 70);
  // Berge
  fill(snow);
  triangle(50, 200, 250, 200, 150, 50);
  triangle(150, 200, 350, 200, 250, 40);
  // Schneescholle
  fill(snow);
  ellipse(300, 310, 130, 90);
}

