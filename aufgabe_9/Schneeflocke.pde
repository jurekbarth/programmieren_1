class Snowflake {
  float x, y;
  Snowflake(float _x, float _y) {
    x = _x;
    y = _y;
  }
  void animate() {
    if (y > 610) {
      y = random(-3, 0);
    } else {
      y += random(0.5, 1.5);
    }
    if (x > 810) {
      x = random(-3, 0);
    } else {
      x += random(0.1, 0.5);
    }
  }
  void paint() {
    fill(#ffffff);
    ellipse(x, y, 10, 10);
  }
}
