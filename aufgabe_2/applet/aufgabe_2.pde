/* Aufgabe: 2 Antarktis
Name: Jurek Barth
Martrikel: 245357
Datum: 19.10.13

Hiermit versichere ich,dass ich diesen Code selbst geschrieben habe.
Er wurde nicht kopiert und auch nich diktiert.
*/

size(800, 600);
noStroke();

// Define Colors:
color sky = color(#AEDAF8);
color white = color(#ffffff);
color snow = color(#F4F7FA);
color black = color(#000000);
color orange = color(#E5861F);
color sea = color(#8AABDA);
color grey = color(#454545);
color yellow = color(#FCF238);


fill(sky);
rect(0, 0, 800, 600);
fill(sea);
rect(0, 200, 800, 400);
fill(snow);
rect(0, 400, 800, 200);
// Sonne
fill(yellow);
ellipse(700, 50, 70, 70);
// Berge
fill(snow);
triangle(50, 200, 250, 200, 150, 50);
triangle(150, 200, 350, 200, 250, 40);
// Schneescholle
fill(snow);
ellipse(300, 310, 130, 90);
fill(black);
// Fuesse
arc(600, 530, 100, 60, PI, PI*2);
// Arme
ellipse(570, 460, 70, 70);
ellipse(630, 460, 70, 70);
// Kopf
ellipse(600, 420, 90, 70);
// Bauch
fill(white);
ellipse(600, 470, 100, 120);
// Augen
ellipse(582, 410, 30, 30);
ellipse(618, 410, 30, 30);
// Pupille
fill(grey);
ellipse(585, 410, 5, 5);
ellipse(615, 410, 5, 5);
// Zunge
fill(orange);
ellipse(600, 415, 10, 10);
// Nase
fill(grey);
arc(600, 415, 22, 22, PI, PI*2);

// Zweiter Pinguin SVG Datei
// Quelle: http://www.clipartsfree.net/svg/9247-tux-the-penguin-vector.html




